var class_clipper_lib_1_1_clipper_base =
[
    [ "AddPath", "class_clipper_lib_1_1_clipper_base.html#a82c93b3cb61cf96836443f9b51076e61", null ],
    [ "AddPaths", "class_clipper_lib_1_1_clipper_base.html#a4682cb8b416761eea4132aab28f59acb", null ],
    [ "Clear", "class_clipper_lib_1_1_clipper_base.html#aba7888aa2c56d767a395d94c8b6f1cec", null ],
    [ "GetBounds", "class_clipper_lib_1_1_clipper_base.html#ac7058458d1a0ca505f8a2459ff7cfd16", null ],
    [ "PopLocalMinima", "class_clipper_lib_1_1_clipper_base.html#a3c13aeed90e7db9bf222546727ed180b", null ],
    [ "Reset", "class_clipper_lib_1_1_clipper_base.html#a899c0b3fa7a81549d5bcc1c3a95b01b1", null ],
    [ "SlopesEqual", "class_clipper_lib_1_1_clipper_base.html#a638f3a0347fe5dee31078fd40f2c7c01", null ],
    [ "SlopesEqual", "class_clipper_lib_1_1_clipper_base.html#a3f1e45302b2c32f9ad065ae617ab2c36", null ],
    [ "horizontal", "class_clipper_lib_1_1_clipper_base.html#a66dcb25eba2d4221d35fba23c926ab62", null ],
    [ "Skip", "class_clipper_lib_1_1_clipper_base.html#a4361a2fcac024883dd82c4d0e100ebcd", null ],
    [ "tolerance", "class_clipper_lib_1_1_clipper_base.html#a6f9770ebc412c3b0525e3c10e9f5cc86", null ],
    [ "Unassigned", "class_clipper_lib_1_1_clipper_base.html#a192e319b2985df73ddf856231965b9c1", null ],
    [ "PreserveCollinear", "class_clipper_lib_1_1_clipper_base.html#a35bb87534c17efc436a16db2cd4684b0", null ]
];