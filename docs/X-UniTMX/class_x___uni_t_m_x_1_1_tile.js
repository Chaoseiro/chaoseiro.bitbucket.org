var class_x___uni_t_m_x_1_1_tile =
[
    [ "Tile", "class_x___uni_t_m_x_1_1_tile.html#a7609ba22209795c2ae391adb4d538968", null ],
    [ "Tile", "class_x___uni_t_m_x_1_1_tile.html#a85b997a6ad62ea73020255c3a27ee13d", null ],
    [ "Clone", "class_x___uni_t_m_x_1_1_tile.html#a6366e700c62f841386a3182be315aaea", null ],
    [ "Clone", "class_x___uni_t_m_x_1_1_tile.html#aeb692a4a30038b3a58d6c9e957911eb7", null ],
    [ "CreateSprite", "class_x___uni_t_m_x_1_1_tile.html#a8a22984f6de6220b110bb65cbea7a143", null ],
    [ "CreateTileObject", "class_x___uni_t_m_x_1_1_tile.html#a6a777ead8f36ebce38169e1315ce1661", null ],
    [ "GetPropertyAsBoolean", "class_x___uni_t_m_x_1_1_tile.html#a25c39f20e536bde10351d36bf0e07922", null ],
    [ "GetPropertyAsFloat", "class_x___uni_t_m_x_1_1_tile.html#ac4560b6245c2216b1e5565c5e519386f", null ],
    [ "GetPropertyAsInt", "class_x___uni_t_m_x_1_1_tile.html#ae9b0a5fb3f648d14f11477504a1cd7a0", null ],
    [ "GetPropertyAsString", "class_x___uni_t_m_x_1_1_tile.html#a2695dbbc6141c3f77201ef502645f0ac", null ],
    [ "HasProperty", "class_x___uni_t_m_x_1_1_tile.html#ae18799665309f3d4cc8e1b0d0a7c39f3", null ],
    [ "Color", "class_x___uni_t_m_x_1_1_tile.html#ab7b34ca1e58c3b94c849500bb5b877aa", null ],
    [ "CurrentID", "class_x___uni_t_m_x_1_1_tile.html#a5d17d1add9c496faff33eea2b42166d1", null ],
    [ "MapTileWidth", "class_x___uni_t_m_x_1_1_tile.html#aa5a423c5d525144274df1b49436dfba2", null ],
    [ "OriginalID", "class_x___uni_t_m_x_1_1_tile.html#a72f5b2160e5370c16c2b534926de491f", null ],
    [ "Properties", "class_x___uni_t_m_x_1_1_tile.html#a33fb85a9363c44e61fb45770975c904c", null ],
    [ "Source", "class_x___uni_t_m_x_1_1_tile.html#a433ee20a77b72c6ef9e5b69372be66bc", null ],
    [ "SpriteEffects", "class_x___uni_t_m_x_1_1_tile.html#a3c6dacb98acb7b4f073469449a5141a2", null ],
    [ "TileGameObject", "class_x___uni_t_m_x_1_1_tile.html#a7673654d598709e0abfa8555bc0b0fc7", null ],
    [ "TileSet", "class_x___uni_t_m_x_1_1_tile.html#a60a6d2bbc4dca990195680bf732f87f5", null ],
    [ "TileSprite", "class_x___uni_t_m_x_1_1_tile.html#a76ecef3e67c709ddd9702ff25d61baaa", null ]
];