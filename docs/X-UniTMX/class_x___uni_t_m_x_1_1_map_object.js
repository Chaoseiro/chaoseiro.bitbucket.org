var class_x___uni_t_m_x_1_1_map_object =
[
    [ "MapObject", "class_x___uni_t_m_x_1_1_map_object.html#ad088833493450df95104baaafc13dad8", null ],
    [ "MapObject", "class_x___uni_t_m_x_1_1_map_object.html#a39b736be2fa627ed47d90594844fa20b", null ],
    [ "MapObject", "class_x___uni_t_m_x_1_1_map_object.html#a086582f2580315c1d4ecf2e445c8f4c5", null ],
    [ "MapObject", "class_x___uni_t_m_x_1_1_map_object.html#a9a6879a08917da5360b8606f9c7cd303", null ],
    [ "CreateTileObject", "class_x___uni_t_m_x_1_1_map_object.html#ab7765a6efca98b86925b507377efbe59", null ],
    [ "GetPropertyAsBoolean", "class_x___uni_t_m_x_1_1_map_object.html#af9fa39c3056631ae90129dd7c1564c1c", null ],
    [ "GetPropertyAsFloat", "class_x___uni_t_m_x_1_1_map_object.html#a032fc3392641e0791deaafb30807d205", null ],
    [ "GetPropertyAsInt", "class_x___uni_t_m_x_1_1_map_object.html#a243a58052559e4dc5be2be71c4a51252", null ],
    [ "GetPropertyAsString", "class_x___uni_t_m_x_1_1_map_object.html#a4109e3b7986f9194892b7ef1aa722d0d", null ],
    [ "HasProperty", "class_x___uni_t_m_x_1_1_map_object.html#a7ca22aa9724ce2b7ddd0988f3230793e", null ],
    [ "GID", "class_x___uni_t_m_x_1_1_map_object.html#a746ccf97d6945e07db2fed5e79b6c822", null ],
    [ "Name", "class_x___uni_t_m_x_1_1_map_object.html#aa12085f6f800c8018008c9ad4d29b624", null ],
    [ "ParentObjectLayer", "class_x___uni_t_m_x_1_1_map_object.html#a581a4980e00b5ec77e321da5f1fced49", null ],
    [ "Properties", "class_x___uni_t_m_x_1_1_map_object.html#aba1419cdb4a6d7e7ac97ca7eda9a1b87", null ],
    [ "Type", "class_x___uni_t_m_x_1_1_map_object.html#a5112b3f68c123b27c5c04a4c4a3c342e", null ],
    [ "Visible", "class_x___uni_t_m_x_1_1_map_object.html#a126e36f9ab27ac67d2a3be300e2352a8", null ]
];