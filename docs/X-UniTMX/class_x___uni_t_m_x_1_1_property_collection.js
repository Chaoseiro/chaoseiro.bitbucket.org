var class_x___uni_t_m_x_1_1_property_collection =
[
    [ "PropertyCollection", "class_x___uni_t_m_x_1_1_property_collection.html#aa1de342ce654aa20b65e11289cc59509", null ],
    [ "Add", "class_x___uni_t_m_x_1_1_property_collection.html#a53bb7e533aa1cff9843531b5af0a675c", null ],
    [ "GetEnumerator", "class_x___uni_t_m_x_1_1_property_collection.html#a8b2315c0787890a2b7d4c80d82096a3e", null ],
    [ "GetPropertyAsBoolean", "class_x___uni_t_m_x_1_1_property_collection.html#a9b4f2d36c49fcecdae5263440be2fc2a", null ],
    [ "GetPropertyAsFloat", "class_x___uni_t_m_x_1_1_property_collection.html#a64f2ead0fbba700228d678c54f879ecd", null ],
    [ "GetPropertyAsInt", "class_x___uni_t_m_x_1_1_property_collection.html#adffd0cd8f290ce478c37642888933ea6", null ],
    [ "GetPropertyAsString", "class_x___uni_t_m_x_1_1_property_collection.html#ad71fe872d1d1b2247ebb97a8761f5c18", null ],
    [ "Remove", "class_x___uni_t_m_x_1_1_property_collection.html#a7eb82516b317e7c7dad2f629037ab364", null ],
    [ "TryGetValue", "class_x___uni_t_m_x_1_1_property_collection.html#a13de4f8008ec447d5b463ceab1f809a5", null ],
    [ "this[string name]", "class_x___uni_t_m_x_1_1_property_collection.html#a518d065561e78aad31388a3d020653d0", null ]
];