var namespace_x___uni_t_m_x_1_1_utils =
[
    [ "AnimatedSprite", "class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html", "class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite" ],
    [ "FixedWidthLabel", "class_x___uni_t_m_x_1_1_utils_1_1_fixed_width_label.html", "class_x___uni_t_m_x_1_1_utils_1_1_fixed_width_label" ],
    [ "MathfExtensions", "class_x___uni_t_m_x_1_1_utils_1_1_mathf_extensions.html", "class_x___uni_t_m_x_1_1_utils_1_1_mathf_extensions" ],
    [ "SortingOrderAutoCalculator", "class_x___uni_t_m_x_1_1_utils_1_1_sorting_order_auto_calculator.html", "class_x___uni_t_m_x_1_1_utils_1_1_sorting_order_auto_calculator" ],
    [ "SpriteFrame", "class_x___uni_t_m_x_1_1_utils_1_1_sprite_frame.html", "class_x___uni_t_m_x_1_1_utils_1_1_sprite_frame" ],
    [ "Task", "class_x___uni_t_m_x_1_1_utils_1_1_task.html", "class_x___uni_t_m_x_1_1_utils_1_1_task" ],
    [ "TaskManager", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager.html", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager" ],
    [ "Triangulator", "class_x___uni_t_m_x_1_1_utils_1_1_triangulator.html", "class_x___uni_t_m_x_1_1_utils_1_1_triangulator" ],
    [ "ZeroIndent", "class_x___uni_t_m_x_1_1_utils_1_1_zero_indent.html", "class_x___uni_t_m_x_1_1_utils_1_1_zero_indent" ]
];