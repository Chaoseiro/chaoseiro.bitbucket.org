var searchData=
[
  ['scaleobject',['ScaleObject',['../class_x___uni_t_m_x_1_1_object.html#aa7106d5e2a56f181eaa6fe68c6980814',1,'X_UniTMX.Object.ScaleObject(float TileWidth, float TileHeight)'],['../class_x___uni_t_m_x_1_1_object.html#a474d58b8ae89d13fd06bdb62d3a4a950',1,'X_UniTMX.Object.ScaleObject(float TileWidth, float TileHeight, Orientation orientation)']]],
  ['setmap',['SetMap',['../class_x___uni_t_m_x_1_1_utils_1_1_sorting_order_auto_calculator.html#a074eb5a1b10382872bff947a2f3dfaef',1,'X_UniTMX::Utils::SortingOrderAutoCalculator']]],
  ['settile',['SetTile',['../class_x___uni_t_m_x_1_1_tile_layer.html#aa9bb6ab1d7a3d46f21c1f5ca14a2bb2c',1,'X_UniTMX.TileLayer.SetTile(int x, int y, int newTileID)'],['../class_x___uni_t_m_x_1_1_tile_layer.html#a2cbd3ca5e2a7909309beabaae5a670aa',1,'X_UniTMX.TileLayer.SetTile(float x, float y, int newTileID)'],['../class_x___uni_t_m_x_1_1_tile_layer.html#ae0836b8dd7adef5e2f42ddf86be97a69',1,'X_UniTMX.TileLayer.SetTile(int x, int y, int newTileID, TileSet tileSet)'],['../class_x___uni_t_m_x_1_1_tile_layer.html#ac044d06497e3ede5093e1a83e3eb391b',1,'X_UniTMX.TileLayer.SetTile(float x, float y, int newTileID, TileSet tileSet)']]],
  ['setvalue',['SetValue',['../class_x___uni_t_m_x_1_1_property.html#aae8cb177f74a2e98ef8b825c86d62967',1,'X_UniTMX.Property.SetValue(int value)'],['../class_x___uni_t_m_x_1_1_property.html#a5d0cbf271c8eb9e7cd4b51451bb523af',1,'X_UniTMX.Property.SetValue(float value)'],['../class_x___uni_t_m_x_1_1_property.html#a1e38ed83e3efd778ac060e5a682155c9',1,'X_UniTMX.Property.SetValue(bool value)'],['../class_x___uni_t_m_x_1_1_property.html#a17a00fb7b9960d732c6035a18e09a125',1,'X_UniTMX.Property.SetValue(string value)']]],
  ['sortingorderautocalculator',['SortingOrderAutoCalculator',['../class_x___uni_t_m_x_1_1_utils_1_1_sorting_order_auto_calculator.html',1,'X_UniTMX::Utils']]],
  ['source',['Source',['../class_x___uni_t_m_x_1_1_tile.html#a433ee20a77b72c6ef9e5b69372be66bc',1,'X_UniTMX::Tile']]],
  ['spacing',['Spacing',['../class_x___uni_t_m_x_1_1_tile_set.html#a39b20d9db9c5b8adb99c19beefd518ac',1,'X_UniTMX::TileSet']]],
  ['sprite',['Sprite',['../class_x___uni_t_m_x_1_1_utils_1_1_sprite_frame.html#a7a91bd25166550d99e58b723a1b6406f',1,'X_UniTMX::Utils::SpriteFrame']]],
  ['spriteanimationmode',['SpriteAnimationMode',['../namespace_x___uni_t_m_x_1_1_utils.html#a7c85b4f5b844b1cb9fef63b3df4d3464',1,'X_UniTMX::Utils']]],
  ['spriteeffects',['SpriteEffects',['../class_x___uni_t_m_x_1_1_tile.html#a3c6dacb98acb7b4f073469449a5141a2',1,'X_UniTMX::Tile']]],
  ['spriteeffects',['SpriteEffects',['../class_x___uni_t_m_x_1_1_sprite_effects.html',1,'X_UniTMX']]],
  ['spriteframe',['SpriteFrame',['../class_x___uni_t_m_x_1_1_utils_1_1_sprite_frame.html#ae5e42d7cb9fbaee5ca1bdb5cc994d6c7',1,'X_UniTMX::Utils::SpriteFrame']]],
  ['spriteframe',['SpriteFrame',['../class_x___uni_t_m_x_1_1_utils_1_1_sprite_frame.html',1,'X_UniTMX::Utils']]],
  ['staggered',['Staggered',['../namespace_x___uni_t_m_x.html#a5e8ebe8393a3fa52caeeb4d6399c047baf421d06a10620aa57ae6ebaec46cffe9',1,'X_UniTMX']]],
  ['start',['Start',['../class_x___uni_t_m_x_1_1_utils_1_1_task.html#a14dc28603f9ef753248390d4e0fe4ebe',1,'X_UniTMX::Utils::Task']]],
  ['startenddelegate',['StartEndDelegate',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#a2829b21968d5738d9b2ff82ac99674ad',1,'X_UniTMX::Utils::AnimatedSprite']]],
  ['stop',['Stop',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#a318f420e1ea9a10055a7aa92619d9b39',1,'X_UniTMX.Utils.AnimatedSprite.Stop()'],['../class_x___uni_t_m_x_1_1_utils_1_1_task.html#a330737745ee630f74bb301886c000786',1,'X_UniTMX.Utils.Task.Stop()']]],
  ['subnodes',['SubNodes',['../class_t_object_1_1_shared_1_1_nano_x_m_l_node.html#a19b0dbcfd1dfb934caca332ad7794f11',1,'TObject::Shared::NanoXMLNode']]]
];
