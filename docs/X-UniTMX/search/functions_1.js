var searchData=
[
  ['calculatemeshtangents',['CalculateMeshTangents',['../class_x___uni_t_m_x_1_1_utils_1_1_mathf_extensions.html#a1b2df0dfb0a292d4f653bd6ab43bc775',1,'X_UniTMX::Utils::MathfExtensions']]],
  ['clone',['Clone',['../class_x___uni_t_m_x_1_1_tile.html#a6366e700c62f841386a3182be315aaea',1,'X_UniTMX.Tile.Clone()'],['../class_x___uni_t_m_x_1_1_tile.html#aeb692a4a30038b3a58d6c9e957911eb7',1,'X_UniTMX.Tile.Clone(Vector2 pivot)']]],
  ['completedelegate',['CompleteDelegate',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#a0cdb642a899a83c42401d45b8d7038f6',1,'X_UniTMX::Utils::AnimatedSprite']]],
  ['createsprite',['CreateSprite',['../class_x___uni_t_m_x_1_1_tile.html#a8a22984f6de6220b110bb65cbea7a143',1,'X_UniTMX::Tile']]],
  ['createtileobject',['CreateTileObject',['../class_x___uni_t_m_x_1_1_map_object.html#ab7765a6efca98b86925b507377efbe59',1,'X_UniTMX.MapObject.CreateTileObject()'],['../class_x___uni_t_m_x_1_1_tile.html#a6a777ead8f36ebce38169e1315ce1661',1,'X_UniTMX.Tile.CreateTileObject()']]]
];
