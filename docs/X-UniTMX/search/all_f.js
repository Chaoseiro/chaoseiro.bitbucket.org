var searchData=
[
  ['rawvalue',['RawValue',['../class_x___uni_t_m_x_1_1_property.html#a2d32d3492e5eca7a26a9e737f6e4d9cd',1,'X_UniTMX::Property']]],
  ['rebuildobjectsproperties',['RebuildObjectsProperties',['../class_x___uni_t_m_x_1_1_tiled_map_objects_window.html#a98648ef972672e564b4951ec6345d176',1,'X_UniTMX::TiledMapObjectsWindow']]],
  ['remove',['Remove',['../class_x___uni_t_m_x_1_1_property_collection.html#a7eb82516b317e7c7dad2f629037ab364',1,'X_UniTMX::PropertyCollection']]],
  ['removeobject',['RemoveObject',['../class_x___uni_t_m_x_1_1_map_object_layer.html#a2a07602635fe691c19ef053888f18896',1,'X_UniTMX.MapObjectLayer.RemoveObject(MapObject mapObject)'],['../class_x___uni_t_m_x_1_1_map_object_layer.html#a0cf211538862b429224ce0bd8b055031',1,'X_UniTMX.MapObjectLayer.RemoveObject(string objectName)']]],
  ['renderorder',['RenderOrder',['../namespace_x___uni_t_m_x.html#a19e9b749dc7a4adf1db32a87da014e86',1,'X_UniTMX']]],
  ['reset',['Reset',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#ace0897cf9be8ce73670dc5c1d9baf213',1,'X_UniTMX::Utils::AnimatedSprite']]],
  ['resume',['Resume',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#afa291243d5064e2a9aab80b320c896fa',1,'X_UniTMX::Utils::AnimatedSprite']]],
  ['reverse_5floop',['REVERSE_LOOP',['../namespace_x___uni_t_m_x_1_1_utils.html#a7c85b4f5b844b1cb9fef63b3df4d3464ac96cac566f81e3688e8494f109728d3a',1,'X_UniTMX::Utils']]],
  ['right_5fdown',['Right_Down',['../namespace_x___uni_t_m_x.html#a19e9b749dc7a4adf1db32a87da014e86a3f05b6e11afc925564bf7eaaeb996d70',1,'X_UniTMX']]],
  ['right_5fup',['Right_Up',['../namespace_x___uni_t_m_x.html#a19e9b749dc7a4adf1db32a87da014e86a20ffb7f71e0ee45010deba9ad521180e',1,'X_UniTMX']]],
  ['rootnode',['RootNode',['../class_t_object_1_1_shared_1_1_nano_x_m_l_document.html#a506da9e2f38e3ee031fc01214f76aa1e',1,'TObject::Shared::NanoXMLDocument']]],
  ['rotatepoint',['RotatePoint',['../class_x___uni_t_m_x_1_1_utils_1_1_mathf_extensions.html#a7cfc2a79a97bc15f0d500008ae24549b',1,'X_UniTMX.Utils.MathfExtensions.RotatePoint(this Vector2 point, Vector2 anchor, float angle)'],['../class_x___uni_t_m_x_1_1_utils_1_1_mathf_extensions.html#a683c5ac46305c7a89fe6b4186a803a9a',1,'X_UniTMX.Utils.MathfExtensions.RotatePoint(this Vector2 point, float anchorX, float anchorY, float angle)'],['../class_x___uni_t_m_x_1_1_utils_1_1_mathf_extensions.html#adec4fee8a14f5c96763e59a15db04880',1,'X_UniTMX.Utils.MathfExtensions.RotatePoint(this Vector3 point, Vector3 anchor, Vector3 angle)']]],
  ['rotation',['Rotation',['../class_x___uni_t_m_x_1_1_object.html#acad28f0fc4fb792bce51185e8b507f2b',1,'X_UniTMX::Object']]],
  ['running',['Running',['../class_x___uni_t_m_x_1_1_utils_1_1_task.html#ab472fa4cec7eb31e796889ed96a25c63',1,'X_UniTMX::Utils::Task']]]
];
