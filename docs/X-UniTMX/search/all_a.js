var searchData=
[
  ['layer',['Layer',['../class_x___uni_t_m_x_1_1_layer.html',1,'X_UniTMX']]],
  ['layerdepth',['LayerDepth',['../class_x___uni_t_m_x_1_1_layer.html#a8fca16b6b769ff2f550ba4f5de0780b3',1,'X_UniTMX::Layer']]],
  ['layerdepthspacing',['LayerDepthSpacing',['../class_x___uni_t_m_x_1_1_map.html#a69f1a303420f57228a7e496b2a9c50d8',1,'X_UniTMX::Map']]],
  ['layergameobject',['LayerGameObject',['../class_x___uni_t_m_x_1_1_layer.html#a70b7fc65c41ffb4758a259ef5a4a6ded',1,'X_UniTMX::Layer']]],
  ['layergameobjects',['LayerGameObjects',['../class_x___uni_t_m_x_1_1_tile_layer.html#ae0c7549c8b3f3aa6de29767f27022d03',1,'X_UniTMX::TileLayer']]],
  ['layers',['Layers',['../class_x___uni_t_m_x_1_1_map.html#aaaa8a4c8c31b21330dc5903ea53a6d27',1,'X_UniTMX::Map']]],
  ['left_5fdown',['Left_Down',['../namespace_x___uni_t_m_x.html#a19e9b749dc7a4adf1db32a87da014e86ac31562160f98e519e224db47375cdae9',1,'X_UniTMX']]],
  ['left_5fup',['Left_Up',['../namespace_x___uni_t_m_x.html#a19e9b749dc7a4adf1db32a87da014e86a80ab8ec70fc1f0f9fe6c3c10c4f22c54',1,'X_UniTMX']]],
  ['loop',['LOOP',['../namespace_x___uni_t_m_x_1_1_utils.html#a7c85b4f5b844b1cb9fef63b3df4d3464a9159b3578e4e1eb31ffdf90acd6f6e40',1,'X_UniTMX::Utils']]],
  ['loopcount',['LoopCount',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#a48b3e03aee4cf703948adf6f56f8cde0',1,'X_UniTMX::Utils::AnimatedSprite']]]
];
