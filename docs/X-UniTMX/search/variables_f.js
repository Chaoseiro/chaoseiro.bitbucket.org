var searchData=
[
  ['texture',['Texture',['../class_x___uni_t_m_x_1_1_image_layer.html#af3842095ba46af2c9ea4b43010e9a832',1,'X_UniTMX.ImageLayer.Texture()'],['../class_x___uni_t_m_x_1_1_tile_set.html#adb11bb684214402e8eb592c04d642c19',1,'X_UniTMX.TileSet.Texture()']]],
  ['tileframes',['TileFrames',['../class_x___uni_t_m_x_1_1_tile_animation.html#abe4b8248a2d7aa88b83eba75c953fedc',1,'X_UniTMX::TileAnimation']]],
  ['tileheight',['TileHeight',['../class_x___uni_t_m_x_1_1_tile_set.html#a0c3462b246e963c0b87c1359fc417644',1,'X_UniTMX::TileSet']]],
  ['tileid',['TileID',['../class_x___uni_t_m_x_1_1_tile_frame.html#a2183dd427a60f4671362b2b28dade9ac',1,'X_UniTMX::TileFrame']]],
  ['tileoffsetx',['TileOffsetX',['../class_x___uni_t_m_x_1_1_tile_set.html#a9050739bbf6668eff3237f14ac47a75c',1,'X_UniTMX::TileSet']]],
  ['tileoffsety',['TileOffsetY',['../class_x___uni_t_m_x_1_1_tile_set.html#a5e767d4f43cd423355bc2a649b8b9cf3',1,'X_UniTMX::TileSet']]],
  ['tileproperties',['TileProperties',['../class_x___uni_t_m_x_1_1_tile_set.html#ab7742bfe5cf5e81770e61ab0cdea8f74',1,'X_UniTMX::TileSet']]],
  ['tiles',['Tiles',['../class_x___uni_t_m_x_1_1_tile_set.html#a19096b0b4275f35aec5132a145657320',1,'X_UniTMX::TileSet']]],
  ['tilesobjects',['TilesObjects',['../class_x___uni_t_m_x_1_1_tile_set.html#a0f32f320f4ce09a886d70637e723971c',1,'X_UniTMX::TileSet']]],
  ['tilewidth',['TileWidth',['../class_x___uni_t_m_x_1_1_tile_set.html#a0631f666063ad24e1e1997d8e197b9bf',1,'X_UniTMX::TileSet']]]
];
