var searchData=
[
  ['image',['Image',['../class_x___uni_t_m_x_1_1_image_layer.html#a91bf0596e4263455e7f320da46a0479f',1,'X_UniTMX.ImageLayer.Image()'],['../class_x___uni_t_m_x_1_1_tile_set.html#a66757433e91c683b8e99a15f5eee0071',1,'X_UniTMX.TileSet.Image()']]],
  ['imagelayer',['ImageLayer',['../class_x___uni_t_m_x_1_1_image_layer.html',1,'X_UniTMX']]],
  ['imagelayer',['ImageLayer',['../class_x___uni_t_m_x_1_1_image_layer.html#af3fa2a4f265f0d37a558fb9e86aeca40',1,'X_UniTMX::ImageLayer']]],
  ['init',['Init',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#ab99fb1b49e36dcaadefa16c5bb8beca8',1,'X_UniTMX.Utils.AnimatedSprite.Init()'],['../class_x___uni_t_m_x_1_1_tiled_map_objects_window.html#a3f64fcac9df582db269e60ff3098b551',1,'X_UniTMX.TiledMapObjectsWindow.Init()']]],
  ['intersectnode',['IntersectNode',['../class_clipper_lib_1_1_intersect_node.html',1,'ClipperLib']]],
  ['intpoint',['IntPoint',['../struct_clipper_lib_1_1_int_point.html',1,'ClipperLib']]],
  ['intrect',['IntRect',['../struct_clipper_lib_1_1_int_rect.html',1,'ClipperLib']]],
  ['isometric',['Isometric',['../namespace_x___uni_t_m_x.html#a5e8ebe8393a3fa52caeeb4d6399c047ba93fe1f7c5ca2e09af063aca96d0625cc',1,'X_UniTMX']]]
];
