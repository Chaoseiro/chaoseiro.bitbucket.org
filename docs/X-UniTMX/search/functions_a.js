var searchData=
[
  ['rebuildobjectsproperties',['RebuildObjectsProperties',['../class_x___uni_t_m_x_1_1_tiled_map_objects_window.html#a98648ef972672e564b4951ec6345d176',1,'X_UniTMX::TiledMapObjectsWindow']]],
  ['remove',['Remove',['../class_x___uni_t_m_x_1_1_property_collection.html#a7eb82516b317e7c7dad2f629037ab364',1,'X_UniTMX::PropertyCollection']]],
  ['removeobject',['RemoveObject',['../class_x___uni_t_m_x_1_1_map_object_layer.html#a2a07602635fe691c19ef053888f18896',1,'X_UniTMX.MapObjectLayer.RemoveObject(MapObject mapObject)'],['../class_x___uni_t_m_x_1_1_map_object_layer.html#a0cf211538862b429224ce0bd8b055031',1,'X_UniTMX.MapObjectLayer.RemoveObject(string objectName)']]],
  ['reset',['Reset',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#ace0897cf9be8ce73670dc5c1d9baf213',1,'X_UniTMX::Utils::AnimatedSprite']]],
  ['resume',['Resume',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#afa291243d5064e2a9aab80b320c896fa',1,'X_UniTMX::Utils::AnimatedSprite']]],
  ['rotatepoint',['RotatePoint',['../class_x___uni_t_m_x_1_1_utils_1_1_mathf_extensions.html#a7cfc2a79a97bc15f0d500008ae24549b',1,'X_UniTMX.Utils.MathfExtensions.RotatePoint(this Vector2 point, Vector2 anchor, float angle)'],['../class_x___uni_t_m_x_1_1_utils_1_1_mathf_extensions.html#a683c5ac46305c7a89fe6b4186a803a9a',1,'X_UniTMX.Utils.MathfExtensions.RotatePoint(this Vector2 point, float anchorX, float anchorY, float angle)'],['../class_x___uni_t_m_x_1_1_utils_1_1_mathf_extensions.html#adec4fee8a14f5c96763e59a15db04880',1,'X_UniTMX.Utils.MathfExtensions.RotatePoint(this Vector3 point, Vector3 anchor, Vector3 angle)']]]
];
