var searchData=
[
  ['task',['Task',['../class_x___uni_t_m_x_1_1_utils_1_1_task.html#a0f1250824656461dd16067f16a68ce78',1,'X_UniTMX::Utils::Task']]],
  ['tile',['Tile',['../class_x___uni_t_m_x_1_1_tile.html#a7609ba22209795c2ae391adb4d538968',1,'X_UniTMX.Tile.Tile(TileSet tileSet, Rect source, int OriginalID)'],['../class_x___uni_t_m_x_1_1_tile.html#a85b997a6ad62ea73020255c3a27ee13d',1,'X_UniTMX.Tile.Tile(TileSet tileSet, Rect source, int OriginalID, PropertyCollection properties, Vector2 pivot, int mapTileWidth=0)']]],
  ['tiledpositiontoworldpoint',['TiledPositionToWorldPoint',['../class_x___uni_t_m_x_1_1_map.html#a1a73d3659c521a04914f3ee6c73294eb',1,'X_UniTMX.Map.TiledPositionToWorldPoint(float posX, float posY, Tile tile=null)'],['../class_x___uni_t_m_x_1_1_map.html#ae4de89faca5ea2462c37f23292db0e88',1,'X_UniTMX.Map.TiledPositionToWorldPoint(float posX, float posY, float posZ, Tile tile=null)'],['../class_x___uni_t_m_x_1_1_map.html#a260b54ae11ddd8b9dcd702722c66f72c',1,'X_UniTMX.Map.TiledPositionToWorldPoint(Vector2 position, Tile tile=null)']]],
  ['tileframe',['TileFrame',['../class_x___uni_t_m_x_1_1_tile_frame.html#ac49adbcf0c1ec335af8ca07f72347f4f',1,'X_UniTMX::TileFrame']]],
  ['tilegrid',['TileGrid',['../class_x___uni_t_m_x_1_1_tile_grid.html#af6106c18e1aae3e95af1a2869a3aba13',1,'X_UniTMX::TileGrid']]],
  ['tilelayer',['TileLayer',['../class_x___uni_t_m_x_1_1_tile_layer.html#a2fda8ad51e5abddcb154a1c44fbe1de4',1,'X_UniTMX::TileLayer']]],
  ['tileobject',['TileObject',['../class_x___uni_t_m_x_1_1_tile_object.html#a36cd0392474bdc68366c4f6e9daa2812',1,'X_UniTMX::TileObject']]],
  ['tileset',['TileSet',['../class_x___uni_t_m_x_1_1_tile_set.html#a9709873c41283f9e349a38244a489b90',1,'X_UniTMX.TileSet.TileSet(NanoXMLNode node, Map map, int firstGID=1)'],['../class_x___uni_t_m_x_1_1_tile_set.html#aea36cdd27f37dc39c3dd40018b568268',1,'X_UniTMX.TileSet.TileSet(NanoXMLNode node, string mapPath, Map map, bool isUsingStreamingPath=false, Action&lt; TileSet &gt; onFinishedLoadingTileSet=null, int firstID=1)']]],
  ['trygetvalue',['TryGetValue',['../class_x___uni_t_m_x_1_1_property_collection.html#a13de4f8008ec447d5b463ceab1f809a5',1,'X_UniTMX::PropertyCollection']]]
];
