var searchData=
[
  ['data',['Data',['../class_x___uni_t_m_x_1_1_tile_layer.html#adb95de2ec91207d051c690b4efde05e8',1,'X_UniTMX::TileLayer']]],
  ['declarations',['Declarations',['../class_t_object_1_1_shared_1_1_nano_x_m_l_document.html#ae403b934b1f995f782c5d7d5ac28bc0a',1,'TObject::Shared::NanoXMLDocument']]],
  ['defaultsortingorder',['DefaultSortingOrder',['../class_x___uni_t_m_x_1_1_map.html#ac93dcaf3c54c4f4d02628236dee274f9',1,'X_UniTMX::Map']]],
  ['destroyonautoremove',['DestroyOnAutoRemove',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#ab93bd06d28ed6d7a8962cee8854330e1',1,'X_UniTMX::Utils::AnimatedSprite']]],
  ['doublepoint',['DoublePoint',['../struct_clipper_lib_1_1_double_point.html',1,'ClipperLib']]],
  ['duration',['Duration',['../class_x___uni_t_m_x_1_1_tile_frame.html#ab71d1d122831a10ecc34144e4aab8a79',1,'X_UniTMX.TileFrame.Duration()'],['../class_x___uni_t_m_x_1_1_utils_1_1_sprite_frame.html#a92f4951397a4dbe46aa9c87adf49784a',1,'X_UniTMX.Utils.SpriteFrame.Duration()']]]
];
