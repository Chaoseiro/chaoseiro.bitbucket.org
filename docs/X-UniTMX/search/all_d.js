var searchData=
[
  ['object',['Object',['../class_x___uni_t_m_x_1_1_object.html',1,'X_UniTMX']]],
  ['object',['Object',['../class_x___uni_t_m_x_1_1_object.html#a651bc989d8991de0308f090c7743d5c6',1,'X_UniTMX::Object']]],
  ['object_5ftype_5fcollider',['Object_Type_Collider',['../class_x___uni_t_m_x_1_1_map.html#afdef51ae873174df05fc10df58f519df',1,'X_UniTMX::Map']]],
  ['object_5ftype_5fnocollider',['Object_Type_NoCollider',['../class_x___uni_t_m_x_1_1_map.html#a69780958652f0d742232511f76f69dcf',1,'X_UniTMX::Map']]],
  ['object_5ftype_5ftrigger',['Object_Type_Trigger',['../class_x___uni_t_m_x_1_1_map.html#a577331d1da01936af445774ff26e2493',1,'X_UniTMX::Map']]],
  ['objects',['Objects',['../class_x___uni_t_m_x_1_1_map_object_layer.html#ae9b3a2cad324da8d39737fab508056a9',1,'X_UniTMX::MapObjectLayer']]],
  ['objecttype',['ObjectType',['../class_x___uni_t_m_x_1_1_object.html#a3dc19d614a516efd33945c760cae0ff2',1,'X_UniTMX.Object.ObjectType()'],['../namespace_x___uni_t_m_x.html#ad7e2ec27c72ded2a27630bcb1a8a407d',1,'X_UniTMX.ObjectType()']]],
  ['offset',['Offset',['../class_x___uni_t_m_x_1_1_utils_1_1_sorting_order_auto_calculator.html#a5b91e66dcbe4b291de8e6118ad53ce20',1,'X_UniTMX::Utils::SortingOrderAutoCalculator']]],
  ['onmapfinishedloading',['OnMapFinishedLoading',['../class_x___uni_t_m_x_1_1_map.html#a8d2d5d0035ba3edc937321b7a60820db',1,'X_UniTMX::Map']]],
  ['opacity',['Opacity',['../class_x___uni_t_m_x_1_1_layer.html#ab48274839c16ba21aaa43be20f9f26af',1,'X_UniTMX::Layer']]],
  ['orientation',['Orientation',['../class_x___uni_t_m_x_1_1_map.html#a4fa3e3f85e0716852f42f286ab2d6415',1,'X_UniTMX.Map.Orientation()'],['../namespace_x___uni_t_m_x.html#a5e8ebe8393a3fa52caeeb4d6399c047b',1,'X_UniTMX.Orientation()']]],
  ['originalid',['OriginalID',['../class_x___uni_t_m_x_1_1_tile.html#a72f5b2160e5370c16c2b534926de491f',1,'X_UniTMX::Tile']]],
  ['orthogonal',['Orthogonal',['../namespace_x___uni_t_m_x.html#a5e8ebe8393a3fa52caeeb4d6399c047ba7b2e80981e360c8634aef96cbcb62e57',1,'X_UniTMX']]]
];
