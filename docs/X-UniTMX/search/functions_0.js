var searchData=
[
  ['add',['Add',['../class_x___uni_t_m_x_1_1_property_collection.html#a53bb7e533aa1cff9843531b5af0a675c',1,'X_UniTMX::PropertyCollection']]],
  ['addboxcollider',['AddBoxCollider',['../class_x___uni_t_m_x_1_1_map.html#a3159498d6b159fe87d104a7c766e0571',1,'X_UniTMX::Map']]],
  ['addcollider',['AddCollider',['../class_x___uni_t_m_x_1_1_map.html#adde1c6e3942c83bbc0f975c032e5c049',1,'X_UniTMX::Map']]],
  ['addellipsecollider',['AddEllipseCollider',['../class_x___uni_t_m_x_1_1_map.html#adf1b4e62c5ccae05db63efbb2cd7c2ad',1,'X_UniTMX::Map']]],
  ['addobject',['AddObject',['../class_x___uni_t_m_x_1_1_map_object_layer.html#a25cc7dc3f013e54f06e132c3b530a740',1,'X_UniTMX::MapObjectLayer']]],
  ['addpolygoncollider',['AddPolygonCollider',['../class_x___uni_t_m_x_1_1_map.html#afd5e36a31a4114f1d8f6e5485c5a2756',1,'X_UniTMX::Map']]],
  ['addpolylinecollider',['AddPolylineCollider',['../class_x___uni_t_m_x_1_1_map.html#ab5c971a5f8c493131bcf5242d76beaa7',1,'X_UniTMX::Map']]],
  ['addprefabs',['AddPrefabs',['../class_x___uni_t_m_x_1_1_map.html#a4ed595b9af5da0be732cf599e246b1d0',1,'X_UniTMX::Map']]],
  ['addspriteframe',['AddSpriteFrame',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#a65cdf8bc70976c9f9c2e670463d1b40a',1,'X_UniTMX::Utils::AnimatedSprite']]],
  ['addtileframe',['AddTileFrame',['../class_x___uni_t_m_x_1_1_tile_animation.html#a112fcee49721609b04befe66e1556b2e',1,'X_UniTMX::TileAnimation']]],
  ['applycustomproperties',['ApplyCustomProperties',['../class_x___uni_t_m_x_1_1_map.html#a0560b211723f42a2795cc2f7a15c4037',1,'X_UniTMX::Map']]]
];
