var searchData=
[
  ['calculatemeshtangents',['CalculateMeshTangents',['../class_x___uni_t_m_x_1_1_utils_1_1_mathf_extensions.html#a1b2df0dfb0a292d4f653bd6ab43bc775',1,'X_UniTMX::Utils::MathfExtensions']]],
  ['cananimate',['CanAnimate',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#acabe56b04b1e38dbcb98fa6774c4b65c',1,'X_UniTMX::Utils::AnimatedSprite']]],
  ['clipper',['Clipper',['../class_clipper_lib_1_1_clipper.html',1,'ClipperLib']]],
  ['clipperbase',['ClipperBase',['../class_clipper_lib_1_1_clipper_base.html',1,'ClipperLib']]],
  ['clipperexception',['ClipperException',['../class_clipper_lib_1_1_clipper_exception.html',1,'ClipperLib']]],
  ['clipperlib',['ClipperLib',['../namespace_clipper_lib.html',1,'']]],
  ['clipperoffset',['ClipperOffset',['../class_clipper_lib_1_1_clipper_offset.html',1,'ClipperLib']]],
  ['clipperscale',['ClipperScale',['../class_x___uni_t_m_x_1_1_tile_object.html#aaf68f9ffb36b40f0e350129b2d1d1cff',1,'X_UniTMX::TileObject']]],
  ['clone',['Clone',['../class_x___uni_t_m_x_1_1_tile.html#a6366e700c62f841386a3182be315aaea',1,'X_UniTMX.Tile.Clone()'],['../class_x___uni_t_m_x_1_1_tile.html#aeb692a4a30038b3a58d6c9e957911eb7',1,'X_UniTMX.Tile.Clone(Vector2 pivot)']]],
  ['color',['Color',['../class_x___uni_t_m_x_1_1_map_object_layer.html#a323a001e6aede6e2626be62989c97514',1,'X_UniTMX.MapObjectLayer.Color()'],['../class_x___uni_t_m_x_1_1_tile.html#ab7b34ca1e58c3b94c849500bb5b877aa',1,'X_UniTMX.Tile.Color()']]],
  ['colorkey',['ColorKey',['../class_x___uni_t_m_x_1_1_image_layer.html#a25e3911e9ef6763b7609f6e8a1ef952d',1,'X_UniTMX.ImageLayer.ColorKey()'],['../class_x___uni_t_m_x_1_1_tile_set.html#a98582c485acb7dae8f6e427cb48f37c0',1,'X_UniTMX.TileSet.ColorKey()']]],
  ['complete',['Complete',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#a86bbcfd4ddfc7038e8e9ac15414a2640',1,'X_UniTMX::Utils::AnimatedSprite']]],
  ['completedelegate',['CompleteDelegate',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#a0cdb642a899a83c42401d45b8d7038f6',1,'X_UniTMX::Utils::AnimatedSprite']]],
  ['createsprite',['CreateSprite',['../class_x___uni_t_m_x_1_1_tile.html#a8a22984f6de6220b110bb65cbea7a143',1,'X_UniTMX::Tile']]],
  ['createtileobject',['CreateTileObject',['../class_x___uni_t_m_x_1_1_map_object.html#ab7765a6efca98b86925b507377efbe59',1,'X_UniTMX.MapObject.CreateTileObject()'],['../class_x___uni_t_m_x_1_1_tile.html#a6a777ead8f36ebce38169e1315ce1661',1,'X_UniTMX.Tile.CreateTileObject()']]],
  ['currentframe',['CurrentFrame',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#afc27e3ff57d5c0e7d7dee432b4b84d0b',1,'X_UniTMX::Utils::AnimatedSprite']]],
  ['currentid',['CurrentID',['../class_x___uni_t_m_x_1_1_tile.html#a5d17d1add9c496faff33eea2b42166d1',1,'X_UniTMX::Tile']]]
];
