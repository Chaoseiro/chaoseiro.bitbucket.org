var searchData=
[
  ['parent',['Parent',['../class_x___uni_t_m_x_1_1_map.html#ae312acdd3d50c9e18b0179dbca3d4f66',1,'X_UniTMX::Map']]],
  ['parentobjectlayer',['ParentObjectLayer',['../class_x___uni_t_m_x_1_1_map_object.html#a581a4980e00b5ec77e321da5f1fced49',1,'X_UniTMX::MapObject']]],
  ['paused',['Paused',['../class_x___uni_t_m_x_1_1_utils_1_1_task.html#a8afbb8535b93c5d13602280cabda13e3',1,'X_UniTMX::Utils::Task']]],
  ['pingpongdirection',['PingPongDirection',['../class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html#a5027b0c7838d20113a0c6d00f38c0101',1,'X_UniTMX::Utils::AnimatedSprite']]],
  ['points',['Points',['../class_x___uni_t_m_x_1_1_object.html#afa9060e3a5e03ca12ae7d53435f71c89',1,'X_UniTMX::Object']]],
  ['properties',['Properties',['../class_x___uni_t_m_x_1_1_layer.html#a31c6deffe00965d5758ca34f4c3279a6',1,'X_UniTMX.Layer.Properties()'],['../class_x___uni_t_m_x_1_1_map.html#aaf418ea18911c3cfe568a167f20467f8',1,'X_UniTMX.Map.Properties()'],['../class_x___uni_t_m_x_1_1_map_object.html#aba1419cdb4a6d7e7ac97ca7eda9a1b87',1,'X_UniTMX.MapObject.Properties()'],['../class_x___uni_t_m_x_1_1_tile.html#a33fb85a9363c44e61fb45770975c904c',1,'X_UniTMX.Tile.Properties()']]]
];
