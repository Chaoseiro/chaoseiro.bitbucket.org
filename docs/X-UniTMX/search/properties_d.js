var searchData=
[
  ['this_5bfloat_20x_2c_20float_20y_5d',['this[float x, float y]',['../class_x___uni_t_m_x_1_1_tile_grid.html#a9ac59239d0144ce516e385e095708744',1,'X_UniTMX::TileGrid']]],
  ['this_5bint_20x_2c_20int_20y_5d',['this[int x, int y]',['../class_x___uni_t_m_x_1_1_tile_grid.html#a1ba300530eba03d181cd375f154193d8',1,'X_UniTMX::TileGrid']]],
  ['this_5bstring_20name_5d',['this[string name]',['../class_x___uni_t_m_x_1_1_property_collection.html#a518d065561e78aad31388a3d020653d0',1,'X_UniTMX::PropertyCollection']]],
  ['this_5bstring_20nodename_5d',['this[string nodeName]',['../class_t_object_1_1_shared_1_1_nano_x_m_l_node.html#ae90fea81161eaf4bdee7e9cacf5b6f25',1,'TObject::Shared::NanoXMLNode']]],
  ['tilegameobject',['TileGameObject',['../class_x___uni_t_m_x_1_1_tile.html#a7673654d598709e0abfa8555bc0b0fc7',1,'X_UniTMX::Tile']]],
  ['tileheight',['TileHeight',['../class_x___uni_t_m_x_1_1_map.html#a1d1acb0cc9b810c25807de60c7bd41c1',1,'X_UniTMX::Map']]],
  ['tiles',['Tiles',['../class_x___uni_t_m_x_1_1_map.html#ab489ec5f0109932975b2411f3ca14c8c',1,'X_UniTMX.Map.Tiles()'],['../class_x___uni_t_m_x_1_1_tile_layer.html#a2db1d5a32be109ed56e7915c70a50ee1',1,'X_UniTMX.TileLayer.Tiles()']]],
  ['tileset',['TileSet',['../class_x___uni_t_m_x_1_1_tile.html#a60a6d2bbc4dca990195680bf732f87f5',1,'X_UniTMX::Tile']]],
  ['tilesets',['TileSets',['../class_x___uni_t_m_x_1_1_map.html#a439463b2647d7c93e2a493fff1912874',1,'X_UniTMX::Map']]],
  ['tilesprite',['TileSprite',['../class_x___uni_t_m_x_1_1_tile.html#a76ecef3e67c709ddd9702ff25d61baaa',1,'X_UniTMX::Tile']]],
  ['tilewidth',['TileWidth',['../class_x___uni_t_m_x_1_1_map.html#a078684ea9f293a925cca001a4505d28d',1,'X_UniTMX::Map']]],
  ['type',['Type',['../class_x___uni_t_m_x_1_1_map_object.html#a5112b3f68c123b27c5c04a4c4a3c342e',1,'X_UniTMX::MapObject']]]
];
