var searchData=
[
  ['backgroundcolor',['BackgroundColor',['../class_x___uni_t_m_x_1_1_map.html#a9fe866d9d2a1d71ff09518cce707372a',1,'X_UniTMX::Map']]],
  ['backward',['BACKWARD',['../namespace_x___uni_t_m_x_1_1_utils.html#a7c85b4f5b844b1cb9fef63b3df4d3464a6377b4908ae38f9a57fe9120cf179eb1',1,'X_UniTMX::Utils']]],
  ['basemap',['BaseMap',['../class_x___uni_t_m_x_1_1_tile_layer.html#a814f47dfb1c4ea0175312317fb225d64',1,'X_UniTMX::TileLayer']]],
  ['basematerials',['BaseMaterials',['../class_x___uni_t_m_x_1_1_tile_layer.html#a026aa056291eb8ab327b4e83fee58a3b',1,'X_UniTMX::TileLayer']]],
  ['basetilematerial',['BaseTileMaterial',['../class_x___uni_t_m_x_1_1_map.html#a93a0762718ae31fe84970d027cc7c6a8',1,'X_UniTMX::Map']]],
  ['bounds',['Bounds',['../class_x___uni_t_m_x_1_1_object.html#ab94b8edcec7c68ddec9f17217bd3a33a',1,'X_UniTMX::Object']]],
  ['box',['Box',['../namespace_x___uni_t_m_x.html#ad7e2ec27c72ded2a27630bcb1a8a407da3cfce651e667ab85486dd42a8185f98a',1,'X_UniTMX']]]
];
