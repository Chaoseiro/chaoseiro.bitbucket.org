var class_x___uni_t_m_x_1_1_map_object_layer =
[
    [ "MapObjectLayer", "class_x___uni_t_m_x_1_1_map_object_layer.html#a63ba8d96c00f25516a77056f64551a32", null ],
    [ "AddObject", "class_x___uni_t_m_x_1_1_map_object_layer.html#a25cc7dc3f013e54f06e132c3b530a740", null ],
    [ "GetObject", "class_x___uni_t_m_x_1_1_map_object_layer.html#af289989adece68605395ec061c2f5500", null ],
    [ "RemoveObject", "class_x___uni_t_m_x_1_1_map_object_layer.html#a2a07602635fe691c19ef053888f18896", null ],
    [ "RemoveObject", "class_x___uni_t_m_x_1_1_map_object_layer.html#a0cf211538862b429224ce0bd8b055031", null ],
    [ "Color", "class_x___uni_t_m_x_1_1_map_object_layer.html#a323a001e6aede6e2626be62989c97514", null ],
    [ "Objects", "class_x___uni_t_m_x_1_1_map_object_layer.html#ae9b3a2cad324da8d39737fab508056a9", null ]
];