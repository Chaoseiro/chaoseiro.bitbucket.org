var class_x___uni_t_m_x_1_1_tile_set =
[
    [ "TileSet", "class_x___uni_t_m_x_1_1_tile_set.html#a9709873c41283f9e349a38244a489b90", null ],
    [ "TileSet", "class_x___uni_t_m_x_1_1_tile_set.html#aea36cdd27f37dc39c3dd40018b568268", null ],
    [ "AnimatedTiles", "class_x___uni_t_m_x_1_1_tile_set.html#a2c064d06726377531aff50e8038ad215", null ],
    [ "ColorKey", "class_x___uni_t_m_x_1_1_tile_set.html#a98582c485acb7dae8f6e427cb48f37c0", null ],
    [ "FirstId", "class_x___uni_t_m_x_1_1_tile_set.html#ab1fa867e0ab25f6370d6c7a7ae976e81", null ],
    [ "Image", "class_x___uni_t_m_x_1_1_tile_set.html#a66757433e91c683b8e99a15f5eee0071", null ],
    [ "Margin", "class_x___uni_t_m_x_1_1_tile_set.html#a37cde6376c068bfa6c28d8d9a5108cce", null ],
    [ "Name", "class_x___uni_t_m_x_1_1_tile_set.html#a91802792a4873eaf0e54e4418f38b196", null ],
    [ "Spacing", "class_x___uni_t_m_x_1_1_tile_set.html#a39b20d9db9c5b8adb99c19beefd518ac", null ],
    [ "Texture", "class_x___uni_t_m_x_1_1_tile_set.html#adb11bb684214402e8eb592c04d642c19", null ],
    [ "TileHeight", "class_x___uni_t_m_x_1_1_tile_set.html#a0c3462b246e963c0b87c1359fc417644", null ],
    [ "TileOffsetX", "class_x___uni_t_m_x_1_1_tile_set.html#a9050739bbf6668eff3237f14ac47a75c", null ],
    [ "TileOffsetY", "class_x___uni_t_m_x_1_1_tile_set.html#a5e767d4f43cd423355bc2a649b8b9cf3", null ],
    [ "TileProperties", "class_x___uni_t_m_x_1_1_tile_set.html#ab7742bfe5cf5e81770e61ab0cdea8f74", null ],
    [ "Tiles", "class_x___uni_t_m_x_1_1_tile_set.html#a19096b0b4275f35aec5132a145657320", null ],
    [ "TilesObjects", "class_x___uni_t_m_x_1_1_tile_set.html#a0f32f320f4ce09a886d70637e723971c", null ],
    [ "TileWidth", "class_x___uni_t_m_x_1_1_tile_set.html#a0631f666063ad24e1e1997d8e197b9bf", null ]
];