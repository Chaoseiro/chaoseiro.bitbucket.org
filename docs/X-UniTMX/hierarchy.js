var hierarchy =
[
    [ "ClipperLib.ClipperBase", "class_clipper_lib_1_1_clipper_base.html", [
      [ "ClipperLib.Clipper", "class_clipper_lib_1_1_clipper.html", null ]
    ] ],
    [ "ClipperLib.ClipperOffset", "class_clipper_lib_1_1_clipper_offset.html", null ],
    [ "ClipperLib.DoublePoint", "struct_clipper_lib_1_1_double_point.html", null ],
    [ "Editor", null, [
      [ "X_UniTMX.TiledMapComponentEditor", "class_x___uni_t_m_x_1_1_tiled_map_component_editor.html", null ]
    ] ],
    [ "EditorWindow", null, [
      [ "X_UniTMX.TiledMapObjectsWindow", "class_x___uni_t_m_x_1_1_tiled_map_objects_window.html", null ]
    ] ],
    [ "Exception", null, [
      [ "ClipperLib.ClipperException", "class_clipper_lib_1_1_clipper_exception.html", null ],
      [ "TObject.Shared.XMLParsingException", "class_t_object_1_1_shared_1_1_x_m_l_parsing_exception.html", null ]
    ] ],
    [ "IComparer< IntersectNode >", null, [
      [ "ClipperLib.MyIntersectNodeSort", "class_clipper_lib_1_1_my_intersect_node_sort.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "X_UniTMX.Utils.FixedWidthLabel", "class_x___uni_t_m_x_1_1_utils_1_1_fixed_width_label.html", null ],
      [ "X_UniTMX.Utils.ZeroIndent", "class_x___uni_t_m_x_1_1_utils_1_1_zero_indent.html", null ]
    ] ],
    [ "IEnumerable< Property >", null, [
      [ "X_UniTMX.PropertyCollection", "class_x___uni_t_m_x_1_1_property_collection.html", null ]
    ] ],
    [ "ClipperLib.IntersectNode", "class_clipper_lib_1_1_intersect_node.html", null ],
    [ "ClipperLib.IntPoint", "struct_clipper_lib_1_1_int_point.html", null ],
    [ "ClipperLib.IntRect", "struct_clipper_lib_1_1_int_rect.html", null ],
    [ "X_UniTMX.Layer", "class_x___uni_t_m_x_1_1_layer.html", [
      [ "X_UniTMX.ImageLayer", "class_x___uni_t_m_x_1_1_image_layer.html", null ],
      [ "X_UniTMX.MapObjectLayer", "class_x___uni_t_m_x_1_1_map_object_layer.html", null ],
      [ "X_UniTMX.TileLayer", "class_x___uni_t_m_x_1_1_tile_layer.html", null ]
    ] ],
    [ "X_UniTMX.Map", "class_x___uni_t_m_x_1_1_map.html", null ],
    [ "X_UniTMX.Utils.MathfExtensions", "class_x___uni_t_m_x_1_1_utils_1_1_mathf_extensions.html", null ],
    [ "MonoBehaviour", null, [
      [ "X_UniTMX.TiledMapComponent", "class_x___uni_t_m_x_1_1_tiled_map_component.html", null ],
      [ "X_UniTMX.Utils.AnimatedSprite", "class_x___uni_t_m_x_1_1_utils_1_1_animated_sprite.html", null ],
      [ "X_UniTMX.Utils.SortingOrderAutoCalculator", "class_x___uni_t_m_x_1_1_utils_1_1_sorting_order_auto_calculator.html", null ],
      [ "X_UniTMX.Utils.TaskManager", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager.html", null ]
    ] ],
    [ "TObject.Shared.NanoXMLAttribute", "class_t_object_1_1_shared_1_1_nano_x_m_l_attribute.html", null ],
    [ "TObject.Shared.NanoXMLBase", "class_t_object_1_1_shared_1_1_nano_x_m_l_base.html", [
      [ "TObject.Shared.NanoXMLDocument", "class_t_object_1_1_shared_1_1_nano_x_m_l_document.html", null ],
      [ "TObject.Shared.NanoXMLNode", "class_t_object_1_1_shared_1_1_nano_x_m_l_node.html", null ]
    ] ],
    [ "X_UniTMX.Object", "class_x___uni_t_m_x_1_1_object.html", [
      [ "X_UniTMX.MapObject", "class_x___uni_t_m_x_1_1_map_object.html", null ],
      [ "X_UniTMX.TileObject", "class_x___uni_t_m_x_1_1_tile_object.html", null ]
    ] ],
    [ "ClipperLib.PolyNode", "class_clipper_lib_1_1_poly_node.html", [
      [ "ClipperLib.PolyTree", "class_clipper_lib_1_1_poly_tree.html", null ]
    ] ],
    [ "X_UniTMX.Property", "class_x___uni_t_m_x_1_1_property.html", null ],
    [ "X_UniTMX.SpriteEffects", "class_x___uni_t_m_x_1_1_sprite_effects.html", null ],
    [ "X_UniTMX.Utils.SpriteFrame", "class_x___uni_t_m_x_1_1_utils_1_1_sprite_frame.html", null ],
    [ "X_UniTMX.Utils.Task", "class_x___uni_t_m_x_1_1_utils_1_1_task.html", null ],
    [ "X_UniTMX.Utils.TaskManager.TaskState", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager_1_1_task_state.html", null ],
    [ "X_UniTMX.Tile", "class_x___uni_t_m_x_1_1_tile.html", null ],
    [ "X_UniTMX.TileAnimation", "class_x___uni_t_m_x_1_1_tile_animation.html", null ],
    [ "X_UniTMX.TileFrame", "class_x___uni_t_m_x_1_1_tile_frame.html", null ],
    [ "X_UniTMX.TileGrid", "class_x___uni_t_m_x_1_1_tile_grid.html", null ],
    [ "X_UniTMX.TileSet", "class_x___uni_t_m_x_1_1_tile_set.html", null ],
    [ "X_UniTMX.Utils.Triangulator", "class_x___uni_t_m_x_1_1_utils_1_1_triangulator.html", null ]
];