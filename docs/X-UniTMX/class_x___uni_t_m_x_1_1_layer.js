var class_x___uni_t_m_x_1_1_layer =
[
    [ "Layer", "class_x___uni_t_m_x_1_1_layer.html#a9b9227cfce7c5996ea0722517c1f7b96", null ],
    [ "GetPropertyAsBoolean", "class_x___uni_t_m_x_1_1_layer.html#a11a68782ee20380e3f63f42722fc6398", null ],
    [ "GetPropertyAsFloat", "class_x___uni_t_m_x_1_1_layer.html#a9617f31664a153b3f1f9ec855c4033cf", null ],
    [ "GetPropertyAsInt", "class_x___uni_t_m_x_1_1_layer.html#afd885ee46685ffddaf42966f4e659765", null ],
    [ "GetPropertyAsString", "class_x___uni_t_m_x_1_1_layer.html#a4347a6a4190c18fcfe5b79670369a328", null ],
    [ "HasProperty", "class_x___uni_t_m_x_1_1_layer.html#a880d71f1c893569591398e087c5347ed", null ],
    [ "Height", "class_x___uni_t_m_x_1_1_layer.html#a2f7120df67d93b37ad05ba02287b044d", null ],
    [ "LayerDepth", "class_x___uni_t_m_x_1_1_layer.html#a8fca16b6b769ff2f550ba4f5de0780b3", null ],
    [ "LayerGameObject", "class_x___uni_t_m_x_1_1_layer.html#a70b7fc65c41ffb4758a259ef5a4a6ded", null ],
    [ "Name", "class_x___uni_t_m_x_1_1_layer.html#a32440eee4e026436dbb5f0c5794f4377", null ],
    [ "Opacity", "class_x___uni_t_m_x_1_1_layer.html#ab48274839c16ba21aaa43be20f9f26af", null ],
    [ "Properties", "class_x___uni_t_m_x_1_1_layer.html#a31c6deffe00965d5758ca34f4c3279a6", null ],
    [ "Visible", "class_x___uni_t_m_x_1_1_layer.html#a775a8d499826230e369641999e7980cf", null ],
    [ "Width", "class_x___uni_t_m_x_1_1_layer.html#a1514426939a01a627a9935e302b9fcf9", null ]
];