var namespace_clipper_lib =
[
    [ "Clipper", "class_clipper_lib_1_1_clipper.html", "class_clipper_lib_1_1_clipper" ],
    [ "ClipperBase", "class_clipper_lib_1_1_clipper_base.html", "class_clipper_lib_1_1_clipper_base" ],
    [ "ClipperException", "class_clipper_lib_1_1_clipper_exception.html", "class_clipper_lib_1_1_clipper_exception" ],
    [ "ClipperOffset", "class_clipper_lib_1_1_clipper_offset.html", "class_clipper_lib_1_1_clipper_offset" ],
    [ "DoublePoint", "struct_clipper_lib_1_1_double_point.html", "struct_clipper_lib_1_1_double_point" ],
    [ "IntersectNode", "class_clipper_lib_1_1_intersect_node.html", null ],
    [ "IntPoint", "struct_clipper_lib_1_1_int_point.html", "struct_clipper_lib_1_1_int_point" ],
    [ "IntRect", "struct_clipper_lib_1_1_int_rect.html", "struct_clipper_lib_1_1_int_rect" ],
    [ "MyIntersectNodeSort", "class_clipper_lib_1_1_my_intersect_node_sort.html", "class_clipper_lib_1_1_my_intersect_node_sort" ],
    [ "PolyNode", "class_clipper_lib_1_1_poly_node.html", "class_clipper_lib_1_1_poly_node" ],
    [ "PolyTree", "class_clipper_lib_1_1_poly_tree.html", "class_clipper_lib_1_1_poly_tree" ]
];