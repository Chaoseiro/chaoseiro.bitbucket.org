var class_clipper_lib_1_1_clipper_offset =
[
    [ "ClipperOffset", "class_clipper_lib_1_1_clipper_offset.html#aec8da6aa87f3ec08d039af2046439550", null ],
    [ "AddPath", "class_clipper_lib_1_1_clipper_offset.html#aac0a5528db4519e67f745497886e184e", null ],
    [ "AddPaths", "class_clipper_lib_1_1_clipper_offset.html#af0b72b3ac10be6148889dc720f3fdac2", null ],
    [ "Clear", "class_clipper_lib_1_1_clipper_offset.html#a43516bedf6c294de19639ff0dc4a487c", null ],
    [ "Execute", "class_clipper_lib_1_1_clipper_offset.html#a18c5424399ac7bc6e2e214ddf058a43e", null ],
    [ "Execute", "class_clipper_lib_1_1_clipper_offset.html#a26aac8064b08edcf06617e2bf0d83561", null ],
    [ "ArcTolerance", "class_clipper_lib_1_1_clipper_offset.html#adb2eb06c13bb7a1084138ffeb6892b77", null ],
    [ "MiterLimit", "class_clipper_lib_1_1_clipper_offset.html#a7fb11c873ea32e0888591f1e7b7698c8", null ]
];