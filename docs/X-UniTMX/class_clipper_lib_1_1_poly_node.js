var class_clipper_lib_1_1_poly_node =
[
    [ "GetNext", "class_clipper_lib_1_1_poly_node.html#aba2911397c2e46ea104a2c1355a8505d", null ],
    [ "ChildCount", "class_clipper_lib_1_1_poly_node.html#a5e94303d82c995c29563d764286a974d", null ],
    [ "Childs", "class_clipper_lib_1_1_poly_node.html#a0068b29abf499271b0f8ec8f68108c06", null ],
    [ "Contour", "class_clipper_lib_1_1_poly_node.html#a3ac07a3bd2f2685258d89a495a5dc4e7", null ],
    [ "IsHole", "class_clipper_lib_1_1_poly_node.html#adc34879f20707d4741227b0b0c7548f6", null ],
    [ "IsOpen", "class_clipper_lib_1_1_poly_node.html#a79c75b25ce13bed591edc115f02dc86d", null ],
    [ "Parent", "class_clipper_lib_1_1_poly_node.html#ad6004b42c4986e2d209e1b5ccebcad68", null ]
];