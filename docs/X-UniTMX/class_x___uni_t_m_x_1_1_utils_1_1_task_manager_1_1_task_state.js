var class_x___uni_t_m_x_1_1_utils_1_1_task_manager_1_1_task_state =
[
    [ "TaskState", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager_1_1_task_state.html#ad215091c6530e8cfc66a13e76306c39b", null ],
    [ "FinishedHandler", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager_1_1_task_state.html#a0347d90388c7ad73b4e594b0294091a8", null ],
    [ "Pause", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager_1_1_task_state.html#a35a62ee183dfb892fb970cdd161c1cd2", null ],
    [ "Start", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager_1_1_task_state.html#a64ac44892e7b073f7a73c47531b2b8a8", null ],
    [ "StartAndWait", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager_1_1_task_state.html#a3fa5fd2c02caea0a7f9afe3672843c26", null ],
    [ "Stop", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager_1_1_task_state.html#aff531461820cab87342db38b31acd7ca", null ],
    [ "Unpause", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager_1_1_task_state.html#ae6368b408d85953151315edb1b344690", null ],
    [ "Paused", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager_1_1_task_state.html#ae2888bc753553112ee6bb313da925884", null ],
    [ "Running", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager_1_1_task_state.html#a5013a76ee7e4d077ec82d1c61de953d2", null ],
    [ "Finished", "class_x___uni_t_m_x_1_1_utils_1_1_task_manager_1_1_task_state.html#aa77e7fb7dcb30811d922e45fc826018b", null ]
];