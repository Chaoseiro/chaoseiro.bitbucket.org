var namespace_t_object_1_1_shared =
[
    [ "NanoXMLAttribute", "class_t_object_1_1_shared_1_1_nano_x_m_l_attribute.html", "class_t_object_1_1_shared_1_1_nano_x_m_l_attribute" ],
    [ "NanoXMLBase", "class_t_object_1_1_shared_1_1_nano_x_m_l_base.html", "class_t_object_1_1_shared_1_1_nano_x_m_l_base" ],
    [ "NanoXMLDocument", "class_t_object_1_1_shared_1_1_nano_x_m_l_document.html", "class_t_object_1_1_shared_1_1_nano_x_m_l_document" ],
    [ "NanoXMLNode", "class_t_object_1_1_shared_1_1_nano_x_m_l_node.html", "class_t_object_1_1_shared_1_1_nano_x_m_l_node" ],
    [ "XMLParsingException", "class_t_object_1_1_shared_1_1_x_m_l_parsing_exception.html", "class_t_object_1_1_shared_1_1_x_m_l_parsing_exception" ]
];